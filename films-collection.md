
# Films Collection

1. Title: Avatar  
   Director: James Cameron  
   Release Year: 2022  
   Genre: Science Fiction  

2. Title: Meurtre et Mystère  
   Director: Jeremy Garelick  
   Release Year: 2022  
   Genre: Comédie  

3. Title: The Colors of Fire  
   Director: Clovis Cornillac  
   Release Year: 2022  
   Genre: Drama  
